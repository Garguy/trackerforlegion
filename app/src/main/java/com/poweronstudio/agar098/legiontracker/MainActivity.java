package com.poweronstudio.agar098.legiontracker;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.poweronstudio.agar098.legiontracker.AlarmClock.AlarmClockActivity;
import com.poweronstudio.agar098.legiontracker.UserAccount.LoginActivity;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, TabLayout.OnTabSelectedListener {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    Context context;
    AlarmManager mAlarmManager;
    TimePicker mTimePicker;
    TextView mUpdateAlarmTV;
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.context = this;

        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        mTimePicker = (TimePicker) findViewById(R.id.mTimePickerLayout);
        mUpdateAlarmTV = (TextView) findViewById(R.id.mAlarmUpdateTV);
        Calendar calendar = Calendar.getInstance();

        tabLayout = (TabLayout) findViewById(R.id.homeScreenTabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.dailyString));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.weeklyString));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.homeScreenViewPager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.addOnTabSelectedListener(this);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);

        //AdMob init
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_login) {
            startActivity(new Intent(this, LoginActivity.class));
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_legal) {
            startActivity(new Intent(this, LegalActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_artifactPower) {
            startActivity(new Intent(this, ArtifactPowerInfo.class));
        } else if (id == R.id.nav_alarm) {
            startActivity(new Intent(this, AlarmClockActivity.class));
        } else if (id == R.id.nav_customToDoList) {
            startActivity(new Intent(this, ToDoList.class));

        } /* else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void clearCheckBoxesWeekly(View view) {
        String clearedWeeklyButton = "Weeklies Cleared";
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, clearedWeeklyButton);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, params);
        Toast.makeText(this, R.string.weekly_cleared_string, Toast.LENGTH_LONG).show();
        //Checkboxes
        CheckBox mCheckBoxWk = (CheckBox) findViewById(R.id.checkBoxWk);
        CheckBox mCheckBoxWk1 = (CheckBox) findViewById(R.id.checkBoxWk1);
        CheckBox mCheckBoxWk2 = (CheckBox) findViewById(R.id.checkBoxWk2);
        CheckBox mCheckBoxWk3 = (CheckBox) findViewById(R.id.checkBoxWk3);

        if(mCheckBoxWk.isChecked()){
            mCheckBoxWk.toggle();
        }

        if(mCheckBoxWk1.isChecked()) {
            mCheckBoxWk1.toggle();
        }

        if(mCheckBoxWk2.isChecked()) {
            mCheckBoxWk2.toggle();
        }

        if(mCheckBoxWk3.isChecked()) {
            mCheckBoxWk3.toggle();
        }
    }

    public void clearCheckBoxesDaily(View view) {
        String clearedButton = "Dailies Cleared";
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, clearedButton);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        Toast.makeText(this, R.string.daily_cleared_string, Toast.LENGTH_LONG).show();
        CheckBox mCheckBoxDy = (CheckBox) findViewById(R.id.checkBoxDy);
        CheckBox mCheckBoxDy1 = (CheckBox) findViewById(R.id.checkBoxDy1);
        CheckBox mCheckBoxDy2 = (CheckBox) findViewById(R.id.checkBoxDy2);
        CheckBox mCheckBoxDy3 = (CheckBox) findViewById(R.id.checkBoxDy3);
        CheckBox mCheckBoxDy4 = (CheckBox) findViewById(R.id.checkBoxDy4);
        CheckBox mCheckBoxDy5 = (CheckBox) findViewById(R.id.checkBoxDy5);
        CheckBox mCheckBoxDy6 = (CheckBox) findViewById(R.id.checkBoxDy6);
        CheckBox mCheckBoxDy7 = (CheckBox) findViewById(R.id.checkBoxDy7);

        if(mCheckBoxDy.isChecked()) {
            mCheckBoxDy.toggle();
        }

        if(mCheckBoxDy1.isChecked()) {
            mCheckBoxDy1.toggle();
        }

        if(mCheckBoxDy.isChecked()) {
            mCheckBoxDy.toggle();
        }

        if(mCheckBoxDy2.isChecked()) {
            mCheckBoxDy2.toggle();
        }

        if(mCheckBoxDy3.isChecked()) {
            mCheckBoxDy3.toggle();
        }

        if(mCheckBoxDy4.isChecked()) {
            mCheckBoxDy4.toggle();
        }

        if(mCheckBoxDy5.isChecked()) {
            mCheckBoxDy5.toggle();
        }

        if(mCheckBoxDy6.isChecked()) {
            mCheckBoxDy6.toggle();
        }

        if(mCheckBoxDy7.isChecked()) {
            mCheckBoxDy7.toggle();
        }
    }
}

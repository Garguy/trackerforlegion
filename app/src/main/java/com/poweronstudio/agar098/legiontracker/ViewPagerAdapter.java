package com.poweronstudio.agar098.legiontracker;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by agar098 on 8/20/2016.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ViewPagerAdapter(FragmentManager fragmentManager, int NumofTabs) {
        super(fragmentManager);
        this.mNumOfTabs = NumofTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                DailyTabFragment tab1 = new DailyTabFragment();
                return tab1;
            case 1:
                WeeklyTabFragment tab2 = new WeeklyTabFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

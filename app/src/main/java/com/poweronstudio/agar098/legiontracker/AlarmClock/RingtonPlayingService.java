package com.poweronstudio.agar098.legiontracker.AlarmClock;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.poweronstudio.agar098.legiontracker.MainActivity;
import com.poweronstudio.agar098.legiontracker.R;

import java.util.Random;

/**
 * Created by agar098 on 8/27/2016.
 */
public class RingtonPlayingService extends Service {
    public static final String TAG = "RingtonPlayingService";

    MediaPlayer mMediaPlayer;
    boolean isRunning;
    int startId;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, " we are in the StartCommand");

        //fetch the extra alarm on/off values
        String state = intent.getExtras().getString("extra");
        //fetch the users spinner choice
        Integer userChoice = intent.getExtras().getInt("ringtoneChoice");

        Log.i(TAG, state);
        Log.i("User choice is ", userChoice.toString());

        //put notification here
        //built in notification to help turn alarm off when it goes off if app isnt open
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        //set up an intent that goes to main Activity
        Intent notificationIntent = new Intent(this.getApplicationContext(), MainActivity.class);
        //set up a pending intent for the notification
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(
                this, 0, notificationIntent, 0);
        // Create the notification users will see
        Notification mNotification = new Notification.Builder(this)
                .setContentTitle("Dailies/Weekly time")
                .setContentText("Cancel")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(notificationPendingIntent)
                .setAutoCancel(true)
                .build();

        //converts extra strings to IDs for on/off
        assert state != null;
        switch (state) {
            case "alarm on":
                startId = 1;
                break;
            case "alarm off":
                startId = 0;
                break;
            default:
                startId = 0;
                break;
        }

        if (!this.isRunning && startId == 1) {
            Log.i("There is no music, ", "and you want start");
            //call command for notification
            notificationManager.notify(0, mNotification);

            if (userChoice == 0) {
                // plays random
                int minNumber = 1;
                int maxNumber = 6;
                Random mRandom = new Random();
                int randomNumber = mRandom.nextInt(maxNumber + minNumber);
                Log.e("random number is ", String.valueOf(randomNumber));
                if (randomNumber == 1) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_03);
                    mMediaPlayer.start();
                } else if (randomNumber == 2) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_04);
                    mMediaPlayer.start();
                } else if (randomNumber == 3) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_05);
                    mMediaPlayer.start();
                } else if (randomNumber == 4) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_10);
                    mMediaPlayer.start();
                } else if (randomNumber == 5) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_13);
                    mMediaPlayer.start();
                } else if (randomNumber == 6) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_18);
                    mMediaPlayer.start();
                }

            } else if (userChoice == 1) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_03);
                mMediaPlayer.start();
            } else if (userChoice == 2) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_04);
                mMediaPlayer.start();
            } else if (userChoice == 3) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_05);
                mMediaPlayer.start();
            } else if (userChoice == 4) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_10);
                mMediaPlayer.start();
            } else if (userChoice == 5) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_13);
                mMediaPlayer.start();
            } else if (userChoice == 6) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_18);
                mMediaPlayer.start();
            }
        } else if (this.isRunning && startId == 0) {
            Log.i("there is music,", "and you want end");
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            this.isRunning = false;
            this.startId = 0;
        } else if (!this.isRunning && startId == 0) {
            Log.i("there is no music, ", "and you want end");
            this.isRunning = false;
            this.startId = 0;
        } else if (this.isRunning && startId == 1) {
            Log.i("there is music, ", "and you want start");
            if (userChoice == 0) {
                // plays random
                int minNumber = 1;
                int maxNumber = 6;
                Random mRandom = new Random();
                int randomNumber = mRandom.nextInt(maxNumber + minNumber);
                Log.e("random number is ", String.valueOf(randomNumber));
                if (randomNumber == 1) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_03);
                    mMediaPlayer.start();
                } else if (randomNumber == 2) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_04);
                    mMediaPlayer.start();
                } else if (randomNumber == 3) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_05);
                    mMediaPlayer.start();
                } else if (randomNumber == 4) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_10);
                    mMediaPlayer.start();
                } else if (randomNumber == 5) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_13);
                    mMediaPlayer.start();
                } else if (randomNumber == 6) {
                    mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_18);
                    mMediaPlayer.start();
                }

            } else if (userChoice == 1) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_03);
                mMediaPlayer.start();
            } else if (userChoice == 2) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_04);
                mMediaPlayer.start();
            } else if (userChoice == 3) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_05);
                mMediaPlayer.start();
            } else if (userChoice == 4) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_10);
                mMediaPlayer.start();
            } else if (userChoice == 5) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_13);
                mMediaPlayer.start();
            } else if (userChoice == 6) {
                mMediaPlayer = MediaPlayer.create(this, R.raw.illidan_18);
                mMediaPlayer.start();
            }
            this.isRunning = true;
            this.startId = 1;
        } else {
            Log.i(TAG, "somehow you reached this");
            Toast.makeText(this, R.string.alarm_error_toast_text, Toast.LENGTH_LONG).show();
        }

        this.isRunning = true;
        this.startId = 0;
        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.isRunning = false;
    }
}

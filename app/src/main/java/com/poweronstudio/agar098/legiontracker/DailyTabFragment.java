package com.poweronstudio.agar098.legiontracker;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Created by agar098 on 8/20/2016.
 */
public class DailyTabFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.daily_tab, container, false);

        //AdMob
        AdView adView = (AdView) rootView.findViewById(R.id.homeAdMob);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)//for emulator
                //for real device
                .addTestDevice("8BC1385E4460201CE53A671CD2E1387A")
                .build();
        adView.loadAd(adRequest);

        return rootView;
    }
}

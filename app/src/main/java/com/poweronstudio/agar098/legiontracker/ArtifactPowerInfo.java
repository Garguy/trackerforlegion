package com.poweronstudio.agar098.legiontracker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

/**
 * Created by agar098 on 8/20/2016.
 */
public class ArtifactPowerInfo extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.artifactpower_info_layout);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView mWoWHeadTV = (TextView) findViewById(R.id.tvAPWoWHead);
        mWoWHeadTV.setClickable(true);
        mWoWHeadTV.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='http://www.wowhead.com/guides/legion/artifact-weapons'>WOWHEAD Artifact Power Guide</a>";
        mWoWHeadTV.setText(Html.fromHtml(text));
    }
}

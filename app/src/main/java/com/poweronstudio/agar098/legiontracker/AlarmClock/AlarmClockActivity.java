package com.poweronstudio.agar098.legiontracker.AlarmClock;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.poweronstudio.agar098.legiontracker.R;

import java.util.Calendar;

/**
 * Created by agar098 on 8/24/2016.
 */
public class AlarmClockActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    AlarmManager mAlarmManager;
    TimePicker mTimePicker;
    TextView mUpdateAlarmTV;
    Button mStartAlarm, mDismissAlarm;
    String mAlarmStartDisplay, mAlarmDismissDisplay;
    int hour, minute;
    PendingIntent mPendingIntent;
    Context context;
    Integer ringtonSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_clock);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.context = this;

        mStartAlarm = (Button) findViewById(R.id.setAlarmBtn);
        mDismissAlarm = (Button) findViewById(R.id.stopAlarmBtn);
        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        mTimePicker = (TimePicker) findViewById(R.id.mTimePickerLayout);
        mUpdateAlarmTV = (TextView) findViewById(R.id.mAlarmUpdateTV);
        final Calendar calendar = Calendar.getInstance();

        final Intent mIntent = new Intent(this.context, AlarmReceiver.class);

        mStartAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    calendar.set(Calendar.HOUR_OF_DAY, mTimePicker.getHour());
                    calendar.set(Calendar.MINUTE, mTimePicker.getMinute());
                    hour = mTimePicker.getHour();
                    minute = mTimePicker.getMinute();
                } else {
                    calendar.set(Calendar.HOUR_OF_DAY, mTimePicker.getCurrentHour());
                    calendar.set(Calendar.MINUTE, mTimePicker.getCurrentMinute());
                    hour = mTimePicker.getCurrentHour();
                    minute = mTimePicker.getCurrentMinute();
                }
                mAlarmStartDisplay = String.valueOf(hour);
                mAlarmDismissDisplay = String.valueOf(minute);

                if (hour > 12) {
                    mAlarmStartDisplay = String.valueOf(hour - 12);
                }
                if (minute < 10) {
                    mAlarmDismissDisplay = "0" + String.valueOf(minute);
                }

                setAlarmText("Alarm set to: " + mAlarmStartDisplay + ":" + mAlarmDismissDisplay);

                //put in Extras into intent, tells the clock "alarm on" was clicked
                mIntent.putExtra("extra", "alarm on");

                //put in extra long into mIntent
                //tells clock that you want a certain ringtone from spinner
                mIntent.putExtra("ringtoneChoice", ringtonSelected);

                //delays the intent until the specified time by user
                mPendingIntent = PendingIntent.getBroadcast(AlarmClockActivity.this, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                //alarm manager set
                mAlarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), mPendingIntent);
            }
        });

        mDismissAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAlarmManager.cancel(mPendingIntent);
                stopAlarmText("Alarm off");

                //put extras in intent, tells the clock "alarm off" was clicked
                mIntent.putExtra("extra", "alarm off");

                //put in extra long into mIntent
                //tells clock that you want a certain ringtone from spinner
                mIntent.putExtra("ringtoneChoice", ringtonSelected);

                //stop the ringtone
                sendBroadcast(mIntent);
            }
        });

        Spinner mSpinner = (Spinner) findViewById(R.id.ringtonSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.alarmSpinner, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(this);
    }

    private void stopAlarmText(String s) {
        mUpdateAlarmTV.setText(s);
    }

    private void setAlarmText(String s) {
        mUpdateAlarmTV.setText(s);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long id) {
        //item was selected. can retrieve the selected item using
        //parent.getItemAtPosition(pos)

        //outputting whatever tone the user has selected in spinner
        ringtonSelected = (int) id;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //Another interface callback
    }
}

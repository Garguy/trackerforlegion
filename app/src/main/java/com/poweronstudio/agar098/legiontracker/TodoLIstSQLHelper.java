package com.poweronstudio.agar098.legiontracker;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by agar098 on 8/27/2016.
 */
class TodoListSQLHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "legiontracker.list.db";
    public static final String TABLE_NAME = "LIST";
    public static final String COL1_TASK = "item";
    public static final String _ID = BaseColumns._ID;

    public TodoListSQLHelper(Context context) {
        //1 is list database version
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createTodoListTable = "CREATE TABLE " + TABLE_NAME +
                " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, " + COL1_TASK + " TEXT)";
        sqLiteDatabase.execSQL(createTodoListTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}

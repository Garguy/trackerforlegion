package com.poweronstudio.agar098.legiontracker.AlarmClock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by agar098 on 8/24/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    public static final String TAG = "AlarmReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "We are in the reciever");

        // fetch extra string from intent
        //tells app which button user pressed, either On or Off
        String mGetAlarmOnString = intent.getExtras().getString("extra");
        Log.i(TAG, "what is the key? " + mGetAlarmOnString);

        //fetch extra longs from alarm clock spinner intent
        //tells which value the user picked from spinner
        Integer getUserAlarmRingToneChoice = intent.getExtras().getInt("ringtoneChoice");
        Log.i("The ringtone choice is ", getUserAlarmRingToneChoice.toString());

        //create intent to the rington service
        Intent mIntent = new Intent(context, RingtonPlayingService.class);

        //pass extra string from AlarmClockActivity to RingtonPlayingService
        mIntent.putExtra("extra", mGetAlarmOnString);

        mIntent.putExtra("ringtoneChoice", getUserAlarmRingToneChoice);
        context.startService(mIntent);
    }
}
